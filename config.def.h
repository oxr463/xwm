/* See LICENSE file for copyright and license details. */

/* appearance */
static const char font[] = "Terminus:size=10";

#define NUMCOLORS	8
static const char colors[NUMCOLORS][ColLast][8]	= {
   /* Border     Foreground Background  */
   { "#060606", "#bbbbbb", "#060606" }, /* Normal */
   { "#111111", "#eeeeee", "#060606" }, /* Selected */
   { "#060606", "#eeeeee", "#060606" }, 
   { "#eeeeee", "#eeeeee", "#060606" },
   { "#eeeeee", "#eeeeee", "#060606" },
   { "#eeeeee", "#eeeeee", "#060606" },
   { "#eeeeee", "#eeeeee", "#060606" },
   { "#eeeeee", "#eeeeee", "#060606" }, /* Window Title Text */
};

//static unsigned int       baralpha           = 0xd0;
//static unsigned int       borderalpha        = OPAQUE;
static const unsigned int borderpx           = 1; /* border pixel of windows */
static const unsigned int snap               = 32; /* snap pixel */
static const Bool         showbar            = True; /* False means no bar */
static const Bool         topbar             = True; /* False means bottom bar */

static const Bool systray_enable  = True;
static const int  systray_spacing = 2;
static const int status_height = 0;

/* tagging */
static const char *tags[]	= { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

// Hide empty tags in tag list
Bool autohide = True;

static const Rule rules[] = {
  /* class       instance    title       tags mask     isfloating   monitor 
    
  { "Firefox",   NULL,        NULL,       1 << 2,       False,       -1 },
  { "Gimp",      NULL,        NULL,       0,            True,        -1 }, */

  { NULL,        NULL,        NULL,       0,            False,       -1 },
};

/* layout(s) */
static const float  mfact       = 0.55; /* 0.55 factor of master area size [0.05..0.95] */
static const Bool   resizehints = False; /* True means respect size hints in tiled resizes */
static const Layout layouts[]   = {
    /* symbol     arrange function */
    { "[M]",       monocle }, 
    { "[F]",       NULL },
    { "[T]",       tile }, /* // Let's hide these extra ones for now.
    { "[=]",       bstackhoriz }, 
    { "[#]" ,      grid },
    { "[||]" ,     bstack }, // */
};

/* key definitions */
#define MODKEY Mod1Mask 
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenu[] = { 
	"dmenu_run" , 
	"-p" , ">>>" ,            /* Prompt */      
	"-fn", font, 
	"-nb", colors[0][ColBG],  /* Normal Background Color */
	"-nf", colors[0][ColFG],  /* Normal Foreground Color */
	"-sb", colors[1][ColBG],  /* Selected Background Color */
	"-sf", colors[1][ColFG],  /* Selected Foreground Color */
	NULL // Don't delete this.
};

static const char *lockscreen[]     = { "slock", NULL };
static const char *screenshot[]     = { "scrot", NULL };
static const char *terminal[]       = { "st",    NULL };
static const char *transparency[]   = { "transset", "-t", "-a", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenu } },	
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = terminal } },	
	{ MODKEY|ControlMask,           XK_l,      spawn,          {.v = lockscreen } },
	{ MODKEY|ShiftMask,             XK_t,      spawn,          {.v = transparency  } },
	{ 0,                            XK_Print,  spawn,          {.v = screenshot } },
	{ MODKEY|ControlMask,           XK_Left,   view_prev_tag,  {0} },
	{ MODKEY|ControlMask,           XK_Right,  view_next_tag,  {0} },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_w,      killclient ,    {0} },		
	
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[0]} },
	
	{ MODKEY,                       XK_space,  setlayout,      {0} }, 
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} }, 
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } }, 
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },	                /* restart xwm on-the-fly */
	{ MODKEY|ControlMask,           XK_q,      spawn,          SHCMD("killall xwm") },	/* kill the xwm session */
	{ MODKEY|ShiftMask,             XK_r,      restart,        {0} },
		
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

};

/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[]															= {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,           0,              Button1,       setlayout,      {0} },
	{ ClkLtSymbol,           0,              Button3,       setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,           0,              Button2,       zoom,           {0} },
	{ ClkStatusText,         0,              Button2,       spawn,          {.v = terminal } },
	{ ClkClientWin,          MODKEY,         Button1,       movemouse,      {0} },
	{ ClkClientWin,          MODKEY,         Button2,       togglefloating, {0} },
	{ ClkClientWin,          MODKEY,         Button3,       resizemouse,    {0} },
	{ ClkTagBar,             0,              Button1,       view,           {0} },
	{ ClkTagBar,             0,              Button3,       toggleview,     {0} },
	{ ClkTagBar,             MODKEY,         Button1,       tag,            {0} },
	{ ClkTagBar,             MODKEY,         Button3,       toggletag,      {0} },

	{ ClkTagBar,             0,              Button4,       view_prev_tag , {0} },
	{ ClkTagBar,             0,              Button5,       view_next_tag , {0} },
	{ ClkWinTitle,           0,              Button4,       focusstack,     {.i = -1 } },
	{ ClkWinTitle,           0,              Button5,       focusstack,     {.i = +1 } },
	{ ClkWinTitle,           0,              Button1,       focusonclick,   {0} },
    { ClkWinTitle,           0 ,             Button3 ,      closeonclick ,  {0} },

};
