# Vagrant

The following command will initialize a virtual machine
for each supported operating system:

```sh
for os in alpine gentoo openbsd; do
  cd "test/vagrant/${os}"
  vagrant up
done
```

