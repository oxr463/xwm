# xwm - extended window manager
# See LICENSE file for copyright and license details.

SRC_DIR = ./src
BUILD_DIR = .

include $(SRC_DIR)/config.mk

SRC = $(SRC_DIR)/xwm.c
OBJ = ${SRC:.c=.o}

all: options config statusbar xwm

options:
	@echo xwm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

${OBJ}: $(SRC_DIR)/config.mk

config:
	@cp $(BUILD_DIR)/config.def.h $(SRC_DIR)/config.h

xwm: ${OBJ}
	@echo CC -o $@
	@${CC} -o $(BUILD_DIR)/$@ ${OBJ} ${LDFLAGS}

statusbar:
	@echo CC -o $@
	@${CC} -o $(BUILD_DIR)/$@ ${SRC_DIR}/statusbar.c ${LDFLAGS}

check:
	@CFLAGS="${CFLAGS} -Wextra -Werror -Wno-sign-compare -fsyntax-only" make
	@echo "checking for errors... OK"

clean:
	@rm -rf $(SRC_DIR)/*.o $(SRC_DIR)/config.h statusbar xwm *.tar.gz

debug:
	@LDFLAGS="${LDFLAGS} -ggdb" make

reformat:
	@VERSION_CONTROL=none $(INDENT) $(SRC_DIR)/*.c $(SRC_DIR)/*.h

dist: clean reformat
	@mkdir -p $(BUILD_DIR)/xwm-${VERSION}
	@cp -R LICENSE Makefile README.md $(SRC_DIR)/config.def.h $(SRC_DIR)/config.mk \
		$(SRC_DIR)/statusbar.c $(SRC_DIR)/xwm.1 ${SRC} $(BUILD_DIR)/xwm-${VERSION}
	@tar -cf $(BUILD_DIR)/xwm-${VERSION}.tar $(BUILD_DIR)/xwm-${VERSION}
	@gzip $(BUILD_DIR)/xwm-${VERSION}.tar
	@rm -rf $(BUILD_DIR)/xwm-${VERSION}

install: all
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f $(BUILD_DIR)/xwm ${DESTDIR}${PREFIX}/bin
	@cp -f $(BUILD_DIR)/statusbar ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/xwm
	@chmod 755 ${DESTDIR}${PREFIX}/bin/statusbar
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed "s/VERSION/${VERSION}/g" < $(SRC_DIR)/xwm.1 > ${DESTDIR}${MANPREFIX}/man1/xwm.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/xwm.1

uninstall:
	@rm -f ${DESTDIR}${PREFIX}/bin/xwm
	@rm -f ${DESTDIR}${PREFIX}/bin/statusbar
	@echo removing manual page from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/xwm.1

.PHONY: all options clean dist install uninstall
