# xwm

_eXtended Window Manager._

## Motivation

The purpose for this project is to extend [dwm][dwm] to include
certain enhancements such as a built-in transparency support,
and both a statusbar and system tray.

## Dependencies

- [Xinerama](https://en.wikipedia.org/wiki/Xinerama) (optional)

- [Xft](https://gitlab.freedesktop.org/xorg/lib/libxft#libxft-x-freetype-library) (optional)

- [Xlib](https://www.x.org/releases/current/doc/libX11/libX11/libX11.html)

## Compiling

```sh
make
```

## Installation

```sh
make install
```

## Acknowledgement

Based on [dwm-plus][dwm-plus], a fork of the venerable [dwm][dwm].

## License

SPDX-License-Identifier: [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)

See [LICENSE](LICENSE) file for copyright and license details.

## Reference

- [Tiling window manager - Wikipedia](https://en.wikipedia.org/wiki/Tiling_window_manager)

## See Also

- [i3](https://i3wm.org)

- [xmonad](https://xmonad.org)

[dwm]: https://dwm.suckless.org
[dwm-plus]: https://code.google.com/archive/p/dwm-plus
